# example-ganttlab

Ganttlab live example. See https://live.ganttlab.org/

## How to use Ganttlab?

Create your own Personal Access Token at `https://gitlab.com/profile/personal_access_tokens`, check only `API`.

Run `ganttlab-live` on your localhost using Docker:

```
$ docker run -p 8181:80 ganttlab/ganttlab-live
```

Visit URL `http://localhost:8181` and use the following login:

  * Your GitLab instance URL: `https://gitlab.com`
  * Personal Access Token: `<your personal access token>`


## PROBLEMS!

Unfortunately, `gitlab` doesn't support Issue `start date`. Only `due date`. It is impossible to list future plans and issues.
